/**
 * Created by koronya on 2016. 9. 28..
 */

var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var methodOverride = require("method-override"); // 1
var app = express();

// DB setting start
// mongoose.connect("mongodb://<ID>:<PW>@ds041486.mlab.com:41486/koro_mongo");
mongoose.connect(process.env.MONGO_DB); // 환경변수에 저장된 값을 사용하여 mongoDB에 접속
var db = mongoose.connection; // mongoose의 db object를 가져와 db변수에 저장
db.once("open", function(){
	console.log("DB connected");
});
db.on("error", function(err){
	console.log("DB ERROR : ", err);
});
// DB setting end


// Other settings
app.set("view engine", "ejs");
app.use(express.static(__dirname+"/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(methodOverride("_method"));

/**
 * _method의 query로 들어오는 값으로 HTTP method를 바꾼다.
 * 예를들어 http://example.com/category/id?_method=delete를 받으면
 * _method의 값인 delete을 읽어 해당 request의 HTTP method를 delete으로 바꾼다
 */
app.use(methodOverride("_method"));

// Routes
app.use("/", require("./routes/home")); //1
app.use("/contacts", require("./routes/contacts")); //2


// Port setting
app.listen(3000, function(){
	console.log("server on!");
});
